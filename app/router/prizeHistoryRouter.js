// Khai báo thư viện express
const express = require('express');

// Import Middleware
const { diceHistoryMiddleware } = require('../middleware/diceHistoryMiddleware');


//Import  Controller
const { createHistory, getAllPrizeHistory, getPrizeHistoryById, updatePrizeHistoryById, deletePrizeHistoryById } = require('../controller/prizeHistorycontroller');

// Tạo router
const prizeHistoryRouter = express.Router();

// Sử dụng middleware
prizeHistoryRouter.use(diceHistoryMiddleware);


// KHAI BÁO API

//CREATE A Dice History
prizeHistoryRouter.post('/prize-history', createHistory);


//GET ALL Dice History
prizeHistoryRouter.get('/prize-history', getAllPrizeHistory);


//GET A Dice History
prizeHistoryRouter.get('/prize-history/:historyId', getPrizeHistoryById);


//UPDATE A Dice History
prizeHistoryRouter.put('/prize-history/:historyId', updatePrizeHistoryById);


//DELETE A Dice History
prizeHistoryRouter.delete('/prize-history/:historyId', deletePrizeHistoryById);


// EXPORT ROUTER
module.exports = prizeHistoryRouter;