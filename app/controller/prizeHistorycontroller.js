// IMPORT dice history model  vào controller
const prizeHistoryModel = require('../model/prizeHistoryModel');

//  Khai báo mongoose
const mongoose = require('mongoose');

// CREATE A DICE HISTORY
const createHistory = (request, response) => {

    //B1: thu thập dữ liệu
    let bodyRequest = request.body;

    //B2: điều kiện dữ liệu
    if (!bodyRequest.user) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "user is required"
        })
    }
    if (!bodyRequest.prize) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "prize is required"
        })
    }

    //B3: Thao tác với cơ sở dữ liệu
    let createPrizeHistory = {
        _id: mongoose.Types.ObjectId(),
        user: bodyRequest.user,
        prize: bodyRequest.prize,
       
    }

    prizeHistoryModel.create(createPrizeHistory, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Create dice history success",
                data: data
            })
        }
    })
}


// LẤY TẤT CẢ DICE HISTORY
const getAllPrizeHistory = (request, response) => {
    //B1: thu thập dữ liệu
    //B2: validate dữ liệu
    //B3: thao tác với cơ sở dữ liệu
    prizeHistoryModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get all prizHistory success",
                data: data
            })
        }
    })
}


// LẤY DICE HISTORY THEO ID
const getPrizeHistoryById = (request, response) => {
    //B1: thu thập dữ liệu
    let historyId = request.params.historyId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(historyId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "Dice History ID is not valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    prizeHistoryModel.findById(historyId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get Dice History by id success",
                data: data
            })
        }
    })
}


//UPDATE A USER
const updatePrizeHistoryById = (request, response) => {
    //B1: thu thập dữ liệu
    let historyId = request.params.historyId;
    let bodyRequest = request.body;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(historyId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "Dice History ID is not valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    let updateprizeHistory = {
        user: bodyRequest.user,
        prize: bodyRequest.prize
    }
    prizeHistoryModel.findByIdAndUpdate(historyId, updateprizeHistory, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update Prize History success",
                data: data
            })
        }
    })
}


// DELETE A USER
const deletePrizeHistoryById = (request, response) => {
    //B1: thu thập dữ liệu
    let historyId = request.params.historyId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(historyId)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "Drink Id is not valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    prizeHistoryModel.findOneAndDelete(historyId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Delete Dice History success"
            })
        }
    })
}
module.exports = { createHistory, getAllPrizeHistory, getPrizeHistoryById, updatePrizeHistoryById, deletePrizeHistoryById };