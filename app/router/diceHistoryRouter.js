// Khai báo thư viện express
const express = require('express');

// Import Middleware
const { diceHistoryMiddleware } = require('../middleware/diceHistoryMiddleware');


//Import  Controller
const { createHistory, getAllDiceHistory, getDiceHistoryById, updateDiceHistoryById, deleteDiceHistoryById } = require('../controller/diceHistoryController');

// Tạo router
const diceHistoryRouter = express.Router();

// Sử dụng middleware
diceHistoryRouter.use(diceHistoryMiddleware);


// KHAI BÁO API

//CREATE A Dice History
diceHistoryRouter.post('/dice-history', createHistory);


//GET ALL Dice History
diceHistoryRouter.get('/dice-history', getAllDiceHistory);


//GET A Dice History
diceHistoryRouter.get('/dice-history/:historyId', getDiceHistoryById);


//UPDATE A Dice History
diceHistoryRouter.put('/dice-history/:historyId', updateDiceHistoryById);


//DELETE A Dice History
diceHistoryRouter.delete('/dice-history/:historyId', deleteDiceHistoryById);


// EXPORT ROUTER
module.exports = diceHistoryRouter;